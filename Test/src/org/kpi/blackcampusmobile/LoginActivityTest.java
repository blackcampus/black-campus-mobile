package org.kpi.blackcampusmobile;

import android.app.Activity;
import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;


import static org.junit.Assert.assertThat;


/**
 * Created by ezalor on 01.08.14.
 */

@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {
    private final ActivityController<LoginActivity> loginActivityActivityController = Robolectric.buildActivity(LoginActivity.class);
    private Activity loginActivity;

    @Before
    public void setUp(){
        loginActivityActivityController.create().start();
        loginActivity = loginActivityActivityController.get();
    }

    @Test
    public void shouldHaveCorrectAppName() {
        assertThat(loginActivity.getResources().getString(R.string.app_name), IsEqual.equalTo("BlackCampusMobile"));
        }
}