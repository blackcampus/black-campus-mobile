package org.kpi.blackcampusmobile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

/**
 * Created by ezalor on 29.07.14.
 */
public class RegistrationActivity extends Activity {

    private static Button registerButton;
    private static RadioButton checkPolicy;
    private static EditText userLogin;
    private static EditText userPassword;
    private static EditText userEmail;
    private static EditText userGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initializeUIElements();
    }

    private void initializeUIElements() {
        registerButton.findViewById(R.id.btnProceed);
        checkPolicy.findViewById(R.id.usrAgreement);
        userLogin.findViewById(R.id.usrLogin);
        userPassword.findViewById(R.id.usrPassword);
        userEmail.findViewById(R.id.usrEmail);
        userGroup.findViewById(R.id.usrGroup);
    }

}
