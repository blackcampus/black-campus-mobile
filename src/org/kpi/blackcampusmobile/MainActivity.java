package org.kpi.blackcampusmobile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by ezalor on 29.07.14.
 */
public class MainActivity extends Activity {

    private static Button browseButton;
    private static Button viewButton;
    private static Button uploadButton;
    private static EditText quickSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUIElements();

    }

    private static void initializeUIElements() {
        browseButton.findViewById(R.id.btnBrowse);
        viewButton.findViewById(R.id.btnView);
        uploadButton.findViewById(R.id.btnUpload);
        quickSearch.findViewById(R.id._quicksearch);
    }
}
