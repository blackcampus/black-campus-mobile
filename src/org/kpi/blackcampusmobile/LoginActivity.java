package org.kpi.blackcampusmobile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {

    private static Button signUpButton;
    private static Button vkSignUpButton;
    private static Button registerButton;
    private static EditText userLogin;
    private static EditText userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
  //      this.initializeUIElements();
    }

    private static void initializeUIElements(){
        signUpButton.findViewById(R.id.btnSignUp);
        vkSignUpButton.findViewById(R.id.btnVKAccount);
        registerButton.findViewById(R.id.btnRegister);
        userLogin.findViewById(R.id.usrLogin);
        userPassword.findViewById(R.id.usrPassword);
    }
}
